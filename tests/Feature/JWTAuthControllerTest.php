<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class JWTAuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testLoginInvalidCredentials()
    {
        $this
            ->postJson('/api/auth/login', [
                'email' => 'email@domain.tld',
                'password' => '123456'
            ])
            ->assertStatus(401);
    }

    // @watch
    public function testLoginSuccess()
    {
        $password = 'test123';
        $user = factory(\App\User::class)->create([
            'password' => bcrypt($password)
        ]);
        $response = $this
            ->postJson('/api/auth/login', [
                'email' => $user->email,
                'password' => $password
            ])
            ->assertSuccessful()
            ->assertJsonStructure([
                'access_token',
                'expires_in',
                'token_type'
            ]);
        $this->assertEquals($response['token_type'], 'bearer');
        $this->assertIsInt($response['expires_in']);
        $this->assertIsString($response['access_token']);
    }

    public function testMeUnauthorized()
    {
        $this->getJson('/api/auth/me')->assertStatus(401);
    }

    public function testMeSuccess()
    {
        $user = factory(\App\User::class)->create();
        $this
            ->actingAs($user, 'api')
            ->getJson('/api/auth/me')
            ->assertSuccessful()
            ->assertExactJson([
                'id' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'created_at' => $user->created_at,
                'email_verified_at' => $user->email_verified_at,
                'updated_at' => $user->updated_at
            ]);
        $this->assertAuthenticated('api');
    }

    public function testRegisterSuccess()
    {
        $password = '123456';
        $user = factory(User::class)->make();
        $this
            ->postJson('/api/auth/register', [
                'email' => $user->email,
                'name' => $user->name,
                'password' => $password
            ])
            ->assertStatus(201);
        $this->assertDatabaseHas('users', $user->only('email'));
        $userPassword = User::where('email', $user->email)
            ->first()
            ->password;
        // $this->assertTrue(Hash::check($password, $userPassword));
    }

    public function testRefreshUnauthorized()
    {
        $this
            ->postJson('/api/auth/refresh')
            ->assertUnauthorized();
    }

    public function testRefreshSuccess()
    {
        $user = factory(\App\User::class)->create();
        $this
            ->actingAs($user, 'api')
            ->postJson('/api/auth/refresh', [])
            ->assertExactJson([ 'a' => 1234])
            ->assertStatus(201);
    }
}
