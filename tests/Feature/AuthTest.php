<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @watch
     * login test
     * @return void
     */
    public function testAuthLoginWeb()
    {
        $password = '123456';
        $user = factory(\App\User::class)->create([
            'password' => bcrypt($password)
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $this->assertAuthenticatedAs($user);
        $response->assertRedirect('/home');
    }
}
